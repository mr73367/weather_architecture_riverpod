
import 'package:dartz/dartz.dart';
import 'package:weather_arch_riverpod/core/error/failure.dart';
import 'package:weather_arch_riverpod/data/weather/remote/api/weather_api.dart';
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';
import 'package:weather_arch_riverpod/domain/weather/repository/weather_repository.dart';

class WeatherRepositoryImpl extends WeatherRepository{
  final WeatherApi weatherApi;

  WeatherRepositoryImpl({required this.weatherApi});
  @override
  Future<Either<ServerFailure, CurrentWeatherEntity>> getCurrentWeather(String city) async{
    try{
      final currentWeather = await weatherApi.getCurrentWeather(city);
      print("run?");
      return Right(currentWeather);
    } catch (e){
      //print("eror");
      print(e.toString());
      return Left(ServerFailure());
    }
  }

}