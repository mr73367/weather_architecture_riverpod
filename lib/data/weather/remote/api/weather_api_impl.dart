
import 'package:dio/dio.dart';
import 'package:weather_arch_riverpod/data/weather/remote/api/weather_api.dart';
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';

import '../../../../core/error/exception.dart';

class WeatherApiImpl extends WeatherApi{
  final Dio dio;

  WeatherApiImpl({required this.dio});

  @override
  Future<CurrentWeatherEntity> getCurrentWeather(String city) async{
    try{
      print(dio.options.baseUrl);
      final response = await dio.get("/forecast.json",queryParameters: {
        "q":city
      });
      return CurrentWeatherEntity.fromJson(response.data["current"]);
    } catch(e){
      throw ServerException(message: e.toString());
    }
  }

}