
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';

abstract class WeatherApi{
    Future<CurrentWeatherEntity> getCurrentWeather(String city);
}