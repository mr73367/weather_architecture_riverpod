import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';
import 'package:weather_arch_riverpod/data/weather/remote/api/weather_api.dart';
import 'package:weather_arch_riverpod/data/weather/remote/api/weather_api_impl.dart';
import 'package:weather_arch_riverpod/data/weather/repository/weather_repository_impl.dart';
import 'package:weather_arch_riverpod/domain/weather/repository/weather_repository.dart';
import 'package:weather_arch_riverpod/domain/weather/usecase/get_current_weather_use_case.dart';
import 'package:weather_arch_riverpod/presentation/weather/weather_page.dart';

import 'core/network/network_dio.dart';

final getIt = GetIt.instance;
Future<void> init()async{
  //network
  getIt.registerLazySingleton<Dio>(() => NetworkDio().provideDio());

  //api data
  getIt.registerLazySingleton<WeatherApi>(() => WeatherApiImpl(dio: getIt()));

  //repository
  getIt.registerLazySingleton<WeatherRepository>(() => WeatherRepositoryImpl(weatherApi: getIt()));

  //usecase
  getIt.registerLazySingleton(() => GetCurrentWeatherUseCase(weatherRepository: getIt()));


}





void main()async {
  await init();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: WeatherPage(),
    );
  }
}
