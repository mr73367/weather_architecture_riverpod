
import 'package:dartz/dartz.dart';
import 'package:weather_arch_riverpod/core/error/failure.dart';
import 'package:weather_arch_riverpod/core/usecase/usecase.dart';
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';
import 'package:weather_arch_riverpod/domain/weather/repository/weather_repository.dart';

class GetCurrentWeatherUseCase extends UseCase<CurrentWeatherEntity,String>{

  final WeatherRepository weatherRepository;

  GetCurrentWeatherUseCase({required this.weatherRepository});

  @override
  Future<Either<ServerFailure, CurrentWeatherEntity>> call(String city) async{
    print("run in usecase");
    return await weatherRepository.getCurrentWeather(city);
  }

}