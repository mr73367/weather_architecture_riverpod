
import 'package:dartz/dartz.dart';
import 'package:weather_arch_riverpod/core/error/failure.dart';
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';

abstract class WeatherRepository{
  Future<Either<ServerFailure,CurrentWeatherEntity>> getCurrentWeather(String city);
}