

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:weather_arch_riverpod/domain/weather/entity/Current_weather_entity.dart';
import 'package:weather_arch_riverpod/domain/weather/usecase/get_current_weather_use_case.dart';
import 'package:weather_arch_riverpod/main.dart';

//final getWeatherUseCaseProvider = Provider((ref) => getIt<GetCurrentWeatherUseCase>());


final currenWeatherProvider = ChangeNotifierProvider((ref) => CurrentWeatherNotifier(getCurrentWeatherUseCase: getIt()));

class CurrentWeatherNotifier extends ChangeNotifier{
  bool isLoading = false;
  CurrentWeatherEntity? currentWeather;
  final GetCurrentWeatherUseCase getCurrentWeatherUseCase;

  CurrentWeatherNotifier({required this.getCurrentWeatherUseCase});

  void getCurrentWeatherEntity({required String city})async{
    isLoading = true;
    notifyListeners();
    final weather = await getCurrentWeatherUseCase(city);
    weather.fold(
            (l) => null,
            (r) {
              currentWeather = r;
            }
    );
    isLoading = false;
    notifyListeners();
  }
}

// class CurrentWeatherNotifier extends ChangeNotifier{
//   CurrentWeatherEntity currentWeather = CurrentWeatherEntity();
//   final GetCurrentWeatherUseCase getCurrentWeatherUseCase;
//
//   CurrentWeatherNotifier({required this.getCurrentWeatherUseCase});
//
//   void getCurrentWeatherEntity({required String city})async{
//     final weather = await getCurrentWeatherUseCase(city);
//     weather.fold(
//             (l) => null,
//             (r) => currentWeather = r
//     );
//     notifyListeners();
//   }
// }

