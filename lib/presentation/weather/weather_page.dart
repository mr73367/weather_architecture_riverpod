import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:weather_arch_riverpod/presentation/weather/providers/weather_provider.dart';


class WeatherPage extends ConsumerWidget {
  WeatherPage({super.key});
  final cityController = TextEditingController();
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final currentWeather = ref.watch(currenWeatherProvider);
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(30.0),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(child: TextField(
                  decoration: const InputDecoration(
                    hintText: "Nhap thanh pho"
                  ),
                  controller: cityController,
                )),
                ElevatedButton(onPressed: (){
                  ref.read(currenWeatherProvider.notifier).getCurrentWeatherEntity(city: cityController.text);
                }, child: Text("Check"))
              ],
            ),
            currentWeather.currentWeather!=null?currentWeather.isLoading?const Center(child: CircularProgressIndicator()):Column(
              children: [
                Text("Thanh pho ${cityController.text}",style: Theme.of(context).textTheme.titleLarge,),
                Text("Nhiet do C: ${currentWeather.currentWeather!.tempC}"),
                Text("Nhiet do F: ${currentWeather.currentWeather!.tempF}"),
              ],
            ):currentWeather.isLoading?const Center(child: CircularProgressIndicator()):Container(),
          ],
        )
      ),
    );
  }
}
