
import 'package:dio/dio.dart';

class NetworkDio{

  final Dio _dio = Dio();
  final defaultNetworkDuration = const Duration(milliseconds: 30000);
  final String _baseUrl = "https://weatherapi-com.p.rapidapi.com";

  BaseOptions _baseOptions(){
    return BaseOptions(
      baseUrl: _baseUrl,
      receiveTimeout: defaultNetworkDuration,
      connectTimeout: defaultNetworkDuration,
      sendTimeout: defaultNetworkDuration,
      headers:{
        'X-RapidAPI-Key': '87e6de1036msh509d37c0911d971p168813jsnc29c7738bbd0',
        'X-RapidAPI-Host': 'weatherapi-com.p.rapidapi.com'
      }
    );
  }

  Dio provideDio(){
    _dio.options = _baseOptions();
    return _dio;
  }

}