
class ServerException implements Exception{
  String message;
  int code;

  ServerException({required this.message, this.code= 0});

  @override
  String toString() {
    return "error: $message";
  }
}